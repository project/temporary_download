<?php

function &_temporary_download_aviable_mail_tokens() {
	
	static $filter = array('site','file','temporary-download', 'user'); 
	$tokens = & drupal_static(__FUNCTION__);
	
	if (!isset($tokens)) {
		
		$tokens = array(); 
		$info = token_info();

		$infotokens = array_filter(
			$info['tokens'], function($key) use ($filter) {return in_array($key, $filter); }, 
			ARRAY_FILTER_USE_KEY
		);  
		
	  foreach($infotokens as $typename => $typetokens) {
	  	foreach($typetokens as $tokenkey => $tokendef) {
	  		$tokens[$typename.':'.$tokenkey] = $tokendef['description'];
	  	}
	  }
	}
	
	return $tokens;
}


/**
 * @file
 * Configuration settings for temporary_download.
 */

 /**
 * @file
 * Admin page callbacks for the temporary_download module.
 */

/**
 * Form builder; Configure the URL alias patterns.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function temporary_download_settings_form($form, $form_state) {
  
	$form = array();
  
  $form['temporary_download_expires_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Default expiration interval'),
    '#default_value' => variable_get('temporary_download_expires_interval', TEMPORARY_DOWNLOAD_DEFAULT_EXPIRES_INTERVAL),
    '#description' => t(
    	'Required format: Ay Bm Cd Dw Eh Fi Gs<br/>'.
    	'A,B,C,D,E,F,G are natural numbers or zero.<br/>'.
    	'The units stands for: y - years, m - months, d - days, w - weeks, h - hours, i - minutes, s - seconds.<br/>'.
    	'Zero values can be omitted.<br/>'.
      'The order and lowercase or uppercase does not matter.<br/>'.
    	'Multiple values of the same unit are added. 1Y 1Y == 2Y.<br/>'
    ),
    '#required' => TRUE,
  );
  
  $form['email_title'] = array(
    '#type' => 'item',
    '#title' => t('E-mails'),
  );
  $form['email'] = array(
    '#type' => 'vertical_tabs',
  );
  // These email tokens are shared for all settings, so just define
  // the list once to help ensure they stay in sync.
  
  
  $form['email_invitation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Invitation to download'),
    '#collapsible' => TRUE,
    '#description' => t('Edit the invitation e-mail messages sent to an invitee.'),
    '#group' => 'email',
  );
  $form['email_invitation']['email_token_help'] = array(
  	'#markup' => t('See: <strong>"Available variables for e-mail"</strong> below.'),
  );
  $form['email_invitation']['temporary_download_mail_invitation_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => _temporary_download_mail_text('invitation_subject', NULL, array(), FALSE),
    '#maxlength' => 180,
  );
  $form['email_invitation']['temporary_download_mail_invitation_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => _temporary_download_mail_text('invitation_body', NULL, array(), FALSE),
    '#rows' => 15,
  );
  
  $form['email_notification'] = array(
    '#type' => 'fieldset',
    '#title' => t('Notification that a download was registered.'),
    '#collapsible' => TRUE,
    '#description' => t('Edit the notification e-mail messages sent to the creator of the download.'),
    '#group' => 'email',
  );
  $form['email_notification']['email_token_help'] = array(
  	'#markup' => t('See: <strong>"Available variables for e-mail"</strong> below.'),
  );
  $form['email_notification']['temporary_download_mail_notification_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => _temporary_download_mail_text('notification_subject', NULL, array(), FALSE),
    '#maxlength' => 180,
  );
  $form['email_notification']['temporary_download_mail_notification_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => _temporary_download_mail_text('notification_body', NULL, array(), FALSE),
    '#rows' => 15,
  );

  $email_token_rows = array();
  foreach(_temporary_download_aviable_mail_tokens() as $name => $description) {
    $email_token_rows[] = array(
      '['.$name.']',
      check_plain($description),
    );
  }
  
  $form['email_variables'] = array(
  	'#type' => 'fieldset',
    '#title' => t('Available variables for e-mail'),
  	'#collapsible' => TRUE,
  	'#collapsed' => TRUE,
  	'table' => array(
  		'#theme' => 'table',
  		'#header' => array( t('Variable'), t('Description') ),
  		'#rows' => $email_token_rows,	
  	),
  );
  
  return system_settings_form($form);
}

function temporary_download_settings_form_validate($form, &$form_state) {
	if (temporary_download_parse_interval($form_state['values']['temporary_download_expires_interval']) === FALSE) {
		form_set_error('temporary_download_expires_interval', 'Invalid format for interval');
	}
}

