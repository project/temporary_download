<?php

function temporary_download_token_info() {
	
	$info = array(
		'types' => array(
			'temporary-download' => array(
				'name' => t('Temporary file download'),
				'description' => t('All fields from a temporary download object'),
			),
		),
		'tokens' => array(
			'temporary-download' => array(
				'url' => array(
					'name' => t('url'),
					'description' => t('Access url of a temporary download.'),
				),
				'token' => array(
					'name' => t('token'),
					'description' => t('Alpha nummeric token of this temporary download. Is part of the access url.'),
				),
				'email' => array(
					'name' => t('e-mail'),
					'description' => t('The e-mail address of the invitee.'),
				),
				'filename' => array(
					'name' => t('filename'),
					'description' => t('The original filename of the download.'),
				),
				'description' => array(
					'name' => t('description'),
					'description' => t('The description of the download.'),
				),
				'downloads' => array(
					'name' => t('downloads'),
					'description' => t('The number of registrated download attempts.'),
				),
				'created' => array(
					'name' => t('created'),
					'description' => t('The date/time when this temporary download was created.'),
				),
				'expires' => array(
					'name' => t('expires'),
					'description' => t('The date/time when this temporary download expires.'),
				),
				'access' => array(
					'name' => t('access'),
					'description' => t('The last date/time when this temporary download was downloaded.'),
				),
				'log-url' => array(
					'name' => t('log-url'),
					'description' => t('URL of the log of registrated download attempts.'),
				),
			),
		),
	);
	
	return $info;
}

function temporary_download_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  $sanitize = !empty($options['sanitize']);
  
  //drupal_set_message($type);
  //drupal_set_message(print_r($data,TRUE));
  
  if ($type == 'temporary-download') {
    foreach($tokens as $name => $original) {
    	$value = '';
    	switch ($name) {
    		case 'url':
    		case 'token':
    		case 'email':
    		case 'filename':
    		case 'downloads':
    			if (!empty($data['temporary-download']->{$name})) {
    				$value = $data['temporary-download']->{$name};
    			}
    			break;
    		case 'description':
    	    if (!empty($data['temporary-download']->description)) {
    				$value = $data['temporary-download']->description;
    			}
    			else {
    			  if (!empty($data['temporary-download']->filename)) {
    			  	$value = $data['temporary-download']->filename;
    			  }
    			}
    			break;
    		case 'created':
    		case 'expires':
    		case 'access':
    			if (intval($data['temporary-download']->{$name})>0) {
    				$value = format_date($data['temporary-download']->{$name},'short');
    			}
    			break;
    		case 'log-url':
    			if (!empty($data['temporary-download']->nid)) {
    				$value = url('node/'.$data['temporary-download']->nid.'/temporary-download/'.$data['temporary-download']->token, array('absolute' => TRUE));
    			}
    			break;
    	}
    	$replacements[$original] = ($sanitize) ? filter_xss($value) : $value;
    }
  }
  
  return $replacements;
}
