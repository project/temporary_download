<?php

/**
 * @file
 * Displays download log of a temporary download links.
 */

 
 /**
 * Form build function for menu local task.
 *
 * @param $form
 * @param $form_state
 * @param $node
 *   A node with attached files.
 * @param $temporary
 *   A token of a temporary download link.
 *
 * @return array
 *   A render array.
 */
function temporary_download_log_form($form, $form_state, $node, $temporary) {

  $form = array();

  $form['operation'] = array(
     '#type' => 'hidden',
     '#value' => 'view',
  );
  
  if (!empty($form_state['triggering_element']) && ($form_state['triggering_element']['#name'] == 'delete')) {  
    $form['operation']['#value'] = 'delete';
  } 

  if ($form['operation']['#value'] == 'delete') {
    $form['confirm'] = array(
      '#markup' => t('You really want delete this temporary link?'),
    	'#prefix' => '<h1>', 	
    	'#suffix' => '</h1>', 	
    );
  }
  
  $header_properties = array(
    'property' => array('data' => t('Property')),
    'value' => array('data' => t('Value')),
  );
  
  $properties = array(
    'filename' => array(
      'title' => t('Filename'),
      'type' => 'string',
    ),
    'description' => array(
      'title' => t('Description'),
      'type' => 'string',
    ),
  	'url' => array(
      'title' => t('Link'),
      'type' => 'link',
    ),
    'email' => array(
      'title' => t('Email'),
      'type' => 'string',
    ),
    'created' => array(
      'title' => t('Created'),
      'type' => 'date',
    ),
    'expires' => array(
      'title' => t('Expires'),
      'type' => 'date',
    ),
    'access' => array(
      'title' => t('Last access'),
      'type' => 'date',
    ),
    'downloads' => array(
      'title' => t('Downloads'),
      'type' => 'int',
    ),
  );
  
  $default_expires = temporary_download_create_expiration_date(time(), variable_get('temporary_download_expires_interval', TEMPORARY_DOWNLOAD_DEFAULT_EXPIRES_INTERVAL));

  // Build the rows of the Table.
  $rows_properties = array();
  if ($temporary) {
    
    $default_expires = intval($temporary->expires);

    foreach($properties as $prop => $args) {
      $row = array('property' => $args['title']);
      switch($args['type']) {
        case 'string':
          $row['value'] = (!empty($temporary->$prop)) ? check_plain($temporary->$prop) : '';
          break;
        case 'link':
          $row['value'] = (!empty($temporary->$prop)) ? l($temporary->$prop,$temporary->$prop) : '';
          break;
        case 'date':
          $row['value'] = (!empty($temporary->$prop) && (intval($temporary->$prop)>0)) ? format_date($temporary->$prop,'short') : '';
          break;
        case 'int':
          $row['value'] = (!empty($temporary->$prop) && (intval($temporary->$prop)>0)) ? intval($temporary->$prop) : '';
          break;
      }
      $rows_properties[] = $row;
    }
  }
  
  // Build the sortable table header.
  $header_attempts = array(
    'timestamp' => array('data' => t('timestamp'), 'field' => 'l.timestamp', 'sort' => 'asc'),
    'address' => array('data' => t('address'), 'field' => 'l.address'),
  );
  
  $query = db_select('temporary_download_log', 'l')
    ->extend('PagerDefault')
    ->extend('TableSort');
    
  $query->innerJoin('temporary_download', 't', 'l.token=t.token AND l.fid=t.fid');
  $query->innerJoin('file_managed', 'f', 'l.fid=f.fid');
  
  $query
    ->fields('l',array('timestamp','address'))
    ->condition('l.token', $temporary->token)
    ->distinct(TRUE)
    ->limit(50)
    ->orderByHeader($header_attempts);
    
  $rslt = $query->execute();
  
  // Build the rows of the Attempts table.
  $rows_attempts = array();
  if ($rslt) {
    while ($col = $rslt->fetchAssoc()) {
      $rows_attempts[] = array(
        'created' => (intval($col['timestamp'])>0) ? format_date($col['timestamp'],'short') : '',
        'address' => check_plain($col['address']),
      );
    }
  }

  $form['property_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Properties of the temporary download link'),
    'properties' => array(
      '#theme' => 'table',
      '#header' => $header_properties,
      '#rows' => $rows_properties,
      '#empty' => t('No content available.'),
    ),
  );
  
  $form['downloads_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Download attempts that was reported'),
    'attempts' => array(
      '#theme' => 'table',
      '#header' => $header_attempts,
      '#rows' => $rows_attempts,
      '#empty' => t('No content available.'),
    ),
  );
  
  $form['expiration_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Date/Time of expiration'),
    'expires' => array(
      '#type' => 'date_popup',
      '#default_value' => format_date($default_expires, 'custom', DATE_FORMAT_DATETIME),
      '#date_type' => DATE_DATETIME,
      '#date_timezone' => date_default_timezone(),
      '#date_format' => temporary_download_datetime_format(),
    ),
    'actions' => array(
      '#type' => 'actions',
      '#attributes' => array('class' => array('container-inline')),
      'expiration' => array(
        '#type' => 'submit',
        '#name' => 'expiration',
        '#value' => t('Set new expiration'),
      ),
    ),
  );
  
  $form['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
    'delete' => array(
      '#type' => 'submit',
      '#name' => 'delete',
      '#value' => t('Delete'),
    ),
    'back' => array(
      '#type' => 'submit',
      '#name' => 'back',
      '#value' => t('Back'),
    ),
  );
  
  return $form;
}

function temporary_download_log_form_submit($form, &$form_state) {
  
  if (!empty($form_state['triggering_element']) && ($form_state['triggering_element']['#name'] == 'expiration')) {  
 
    $node = $form_state['build_info']['args'][0];
    $temporary = $form_state['build_info']['args'][1];
    
    $objExpires = new DateObject( $form_state['values']['expires'], date_default_timezone(TRUE));
    $expires = $objExpires->format('U');

    db_update('temporary_download')
      ->fields(array(
      'expires' => intval($expires),
    ))
    ->condition('token', $temporary->token)
    ->execute();
    $temporary->expires = intval($expires);
    drupal_static_reset('temporary_download_load');
    
    temporary_download_send_mail('invitation', $temporary);  
    
    $form_state['rebuild'] = TRUE;
    return;
  }
  
  if (!empty($form_state['triggering_element']) && ($form_state['triggering_element']['#name'] == 'delete')) {  

    $node = $form_state['build_info']['args'][0];
    $temporary = $form_state['build_info']['args'][1];

    if ($form_state['values']['operation'] == 'delete') {
      db_delete('temporary_download')->condition('token', $temporary->token)->execute();
      db_delete('temporary_download_log')->condition('token', $temporary->token)->execute();
      drupal_static_reset('temporary_download_load');
      return;
    }
    
    $form_state['rebuild'] = TRUE;
    return;
  }

}