<?php

/**
 * @file
 * Tests for temporary_download.module.
 */

/**
 * Test serving temporary downloads.
 */
class TemporaryDownloadTestCase extends FileTestCase {

  public static function getInfo() {
    return array(
      'name' => 'temporary file download',
      'description' => 'Tests for file download/transfer functions.',
      'group' => 'Temporary File Download',
    );
  }

  function setUp() {
    $modules[] = 'temporary_download';
    parent::setUp($modules);
  }

  /**
   * Test the private file transfer system.
   */
  function testPrivateFileTransfer() {

  	// test parsing of intervals
  	$str = '1Y 2M 3D 4H 5I 6S';
  	$obj = temporary_download_parse_interval($str);
  	$this->verbose('Interval created: ' . var_export($obj, TRUE));
  	$this->assertTrue(
  		(($obj->y == 1) && ($obj->m == 2) && ($obj->d == 3) && ($obj->h == 4) && ($obj->i == 5) && ($obj->s == 6)), 
  		"temporary_download_parse_interval($str) is OK"
  	);
  	
  	$str = '1y 2m 3d 4h 5i 6s';
  	$obj = temporary_download_parse_interval($str);
  	$this->verbose('Interval created: ' . var_export($obj, TRUE));
  	$this->assertTrue(
  		(($obj->y == 1) && ($obj->m == 2) && ($obj->d == 3) && ($obj->h == 4) && ($obj->i == 5) && ($obj->s == 6)), 
  		"temporary_download_parse_interval($str) is OK"
  	);
  	
  	$str = '1w 2W';
  	$obj = temporary_download_parse_interval($str);
  	$this->verbose('Interval created: ' . var_export($obj, TRUE));
  	$this->assertTrue(
  		$obj->d == 21, 
  		"temporary_download_parse_interval($str) is OK"
  	);
  	
  	// test calculation of expiration date
  	$time = temporary_download_create_expiration_date(NULL,NULL);
  	$this->assertFalse($time, "temporary_download_create_expiration_date() is OK");
  	
  	$str = '1w';
  	$now = time();
  	$wanted = $now + intval(7*86400);
  	$intvobj = temporary_download_parse_interval('1w');
  	$time = temporary_download_create_expiration_date($now,$intvobj);
  	$this->assertTrue(($wanted+1 >= $time) && ($time+1 >= $wanted), "temporary_download_create_expiration_date(".intval($now).",interval) is OK");
  	
  	$str = '1w';
  	$now = time();
  	$wanted = $now + intval(7*86400);
  	$time = temporary_download_create_expiration_date($now,$str);
  	$this->assertTrue(($wanted+1 >= $time) && ($time+1 >= $wanted), "temporary_download_create_expiration_date(".intval($now).",$str) is OK");
  	
    // Create a file.
    $contents = $this->randomName(8);
    $file = $this->createFile(NULL, $contents, 'private');
    $url = file_create_url($file->uri);
    
    // Test NO access to private file 
    $this->drupalHead($url);
    $this->assertResponse(403, 'Correctly denied access to a private file.');
  	// Register file for temporary download
    $expires = time()+5;
    $token = temporary_download_register_file($file->fid, $expires, NULL, NULL);
    $this->assertFalse(empty($token), 'File token was created');

    // First attempt within 5 seconds
    $this->drupalGet('temporary-download/' . $token);
    $this->assertResponse(200, 'First time download worked');
    $this->assertEqual($contents, $this->content, 'Contents of the file are correct.');
    
    // second attempt after 5 secondes, should be failed
    sleep(6);
    $this->drupalGet('temporary-download/' . $token);
    $this->assertResponse(403, 'Correctly denied access to a private file after expiration.');
   
    // Test deleting a file via API.
    $contents = $this->randomName(8);
    $file = $this->createFile(NULL, $contents, 'private');
    $token = temporary_download_register_file($file->fid, time()+86400, NULL, NULL);
    $temporary = temporary_download_load($token);
    $this->assertFalse(empty($temporary->file));
    file_delete($file);
    $this->refreshVariables(); // Ensure that any changes to variables in the other thread are picked up.
    $this->drupalGet('temporary-download/' . $token);
    $this->assertResponse(404, 'Deleted file not found');
    
    // Test deleting a file directly from disk.
    $contents = $this->randomName(8);
    $file = $this->createFile(NULL, $contents, 'private');
    $token = temporary_download_register_file($file->fid, time()+86400, NULL, NULL);
    $temporary = temporary_download_load($token);
    $this->assertFalse(empty($temporary->file));
    drupal_unlink($file->uri);
    $this->drupalGet('temporary-download/' . $token);
    $this->assertResponse(403, 'Deleted file not accessible');
  }
}

