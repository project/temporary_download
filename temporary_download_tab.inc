<?php

/**
 * @file
 * Displays available temporary download links.
 */

/**
 * Form build function for menu local task.
 *
 * @param $form
 * @param $form_state
 * @param $node
 *   A node with attached files.
 *
 * @return array
 *   A render array.
 */
function temporary_download_tab_form($form, $form_state, $node) {

  if (isset($form_state['values']['operation']) && $form_state['values']['operation'] == 'delete') {
    $tokens = array_keys(array_filter($form_state['values']['tokens']));
    return temporary_download_multiple_delete_confirm($form, $form_state, $node, $tokens);
  }
  
  $form = array();
  
  // Build the 'Update options' form.
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#attributes' => array('class' => array('container-inline')),
  );
  $options = array(
     'do_nothing' => t('Select an operation'),
     'delete' => t('Delete the temporary link'),
     'prolong_one_day' => t('Prolong the temporary link for one day'),
     'prolong_three_days' => t('Prolong the temporary link for three days'),
     'prolong_one_week' => t('Prolong the temporary link for one week'),
  );
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#default_value' => 'do_nothing',
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  // Build the sortable table header.
  $header = array(
    'filename' => array('data' => t('Filename'), 'field' => 'f.filename'),
    'description' => array('data' => t('Description'), 'field' => 't.description'),
  	'email' => array('data' => t('eMail'), 'field' => 't.email'),
    'token' => array('data' => t('Token'), 'field' => 't.token'),
    'created' => array('data' => t('Created'), 'field' => 't.created', 'sort' => 'asc'),
    'expires' => array('data' => t('Expires'), 'field' => 't.expires'),
    'access' => array('data' => t('Last access'), 'field' => 'a.access'),
    'downloads' => array('data' => t('Downloads'), 'field' => 'a.downloads'),
    'operations' => array('data' => t('Operations')),
  );

  $fids = array_keys(temporary_download_node_files($node));
  // drupal_set_message(print_r($fids, TRUE));
  
  $query = db_select('temporary_download', 't')
    ->extend('PagerDefault')
    ->extend('TableSort');
    
  $cntqry = db_select('temporary_download_log', 'l');
  $cntqry->addExpression('count(*)', 'downloads');
  $cntqry->addExpression('max(timestamp)', 'access');
  $cntqry
    ->fields('l',array('token'))
    ->groupBy('token');
    
  $query->innerJoin('file_managed', 'f', 't.fid=f.fid');
  $query->leftJoin($cntqry, 'a', 't.token=a.token');
    
  $query
    ->fields('t',array('token','email','created','expires','description'))
    ->fields('f',array('filename'))
    ->fields('a',array('downloads', 'access'))
    ->condition('t.fid', $fids, 'IN')
    ->distinct(TRUE)
    ->limit(50)
    ->orderByHeader($header);
    
  $rslt = $query->execute();
  
  // Build the rows of the Table.
  $options = array();
  $destination = drupal_get_destination();
  
  if ($rslt) {
    while ($obj = $rslt->fetchObject()) {
      
      $options[$obj->token] = array(
        'filename' => check_plain($obj->filename),
        'description' => check_plain($obj->description),
      	'token' => l(check_plain($obj->token), temporary_download_get_url($obj->token)),
        'email' => check_plain($obj->email),
        'created' => (intval($obj->created)>0) ? format_date($obj->created,'short') : '',
        'expires' => (intval($obj->expires)>0) ? format_date($obj->expires,'short') : '',
        'access' => (intval($obj->access)>0) ? format_date($obj->access,'short') : '',
        'downloads' => check_plain($obj->downloads),
      );
      
      $operations = array(
        'details' => array(
          'title' => t('details'),
          'href' => 'node/'.$node->nid.'/temporary-download/'.$obj->token,
          'query' => $destination,
        )
      );
      
      $options[$obj->token]['operations'] = array();
      if (count($operations) > 1) {
        // Render an unordered list of operations links.
        $options[$obj->token]['operations'] = array(
          'data' => array(
            '#theme' => 'links__node_operations',
            '#links' => $operations,
            '#attributes' => array('class' => array('links', 'inline')),
          ),
        );
      }
      elseif (!empty($operations)) {
        // Render the first and only operation as a link.
        $link = reset($operations);
        $options[$obj->token]['operations'] = array(
          'data' => array(
            '#type' => 'link',
            '#title' => $link['title'],
            '#href' => $link['href'],
            '#options' => array('query' => $link['query']),
          ),
        );
      }
    }
  }

  $form['tokens'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No content available.'),
  );
  
  $form['pager'] = array('#markup' => theme('pager'));
  
  return $form;
}

/**
 * Form validate function for operation.
 *
 * @param $form
 * @param $form_state
 */
function temporary_download_tab_form_validate($form, &$form_state) {
  
  if ($form_state['values']['operation'] != 'do_nothing') {
    
    // Error if there are no items to select.
    if (!is_array($form_state['values']['tokens']) || !count(array_filter($form_state['values']['tokens']))) {
      form_set_error('', t('No items selected.'));
    }
  }
}

/**
 * Form submit function for operation.
 *
 * @param $form
 * @param $form_state
 */
function temporary_download_tab_form_submit($form, &$form_state) {

  $node = $form_state['build_info']['args'][0];
  $tokens = array_keys(array_filter($form_state['values']['tokens']));
  
  if ($form_state['values']['operation'] == 'delete') {
    $form_state['rebuild'] = TRUE;
    return;
  }      

  if ($form_state['values']['operation'] == 'prolong_one_day') {
    foreach($tokens as $token) {
      db_update('temporary_download')
        ->expression('expires', 'expires + 86400')
        ->condition('token', $token)
        ->execute();
    }
  }      

  if ($form_state['values']['operation'] == 'prolong_three_days') {
    foreach($tokens as $token) {
      db_update('temporary_download')
        ->expression('expires', 'expires + '.intval(86400*3))
        ->condition('token', $token)
        ->execute();
    }
  }      

  if ($form_state['values']['operation'] == 'prolong_one_week') {
    foreach($tokens as $token) {
      db_update('temporary_download')
        ->expression('expires', 'expires + '.intval(86400*7))
        ->condition('token', $token)
        ->execute();
    }
  }          
}

/**
 * Form build function for delete confirmation.
 *
 * @param $form
 * @param $form_state
 * @param $node
 *   A node with attached files.
 * @param $tokens
 *   An array of tokens of temporary links that will be deleted
 *
 * @return array
 *   A render array.
 */
function temporary_download_multiple_delete_confirm($form, $form_state, $node, $tokens) {
  
  $form = array();
  
  // Build the sortable table header.
  $header = array(
    'filename' => array('data' => t('Filename')),
    'token' => array('data' => t('Token')),
    'email' => array('data' => t('eMail')),
    'created' => array('data' => t('created')),
    'expires' => array('data' => t('expires')),
    'access' => array('data' => t('last access')),
    'downloads' => array('data' => t('downloads')),
  );

  $fids = array_keys(temporary_download_node_files($node));
  // drupal_set_message(print_r($fids, TRUE));
  
  $query = db_select('temporary_download', 't');
    
  $cntqry = db_select('temporary_download_log', 'l');
  $cntqry->addExpression('count(*)', 'downloads');
  $cntqry->addExpression('max(timestamp)', 'access');
  $cntqry
    ->fields('l',array('token'))
    ->groupBy('token');
    
  $query->innerJoin('file_managed', 'f', 't.fid=f.fid');
  $query->leftJoin($cntqry, 'a', 't.token=a.token');
    
  $query
    ->fields('t',array('token','email','created','expires'))
    ->fields('f',array('filename'))
    ->fields('a',array('downloads', 'access'))
    ->condition('t.fid', $fids, 'IN')
    ->distinct(TRUE);
    
  $rslt = $query->execute();

  $form['operation'] = array(
    '#type' => 'hidden',
    '#value' => 'delete',
  );

  $form['tokens'] = array(
    '#tree' => TRUE,
  );
  
  // Build the rows of the Table.
  $rows = array();
  if ($rslt) {
    while ($obj = $rslt->fetchObject()) {
      if (in_array($obj->token, $tokens)) {
        
        $form['tokens'][$obj->token] = array(
          '#type' => 'hidden',
          '#value' => $obj->token,
        );
        
        $rows[$obj->token] = array(
          'filename' => check_plain($obj->filename),
          'token' => l(check_plain($obj->token), 'temporary-download/'.$obj->token, array('absolute' => TRUE)),
          'email' => check_plain($obj->email),
          'created' => (intval($obj->created)>0) ? format_date($obj->created,'short') : '',
          'expires' => (intval($obj->expires)>0) ? format_date($obj->expires,'short') : '',
          'access' => (intval($obj->access)>0) ? format_date($obj->access,'short') : '',
          'downloads' => check_plain($obj->downloads),
        );
        
      }
    }
  }
  
  $confirm_question = format_plural(count($tokens),
    'Are you sure you want to delete this link?',
    'Are you sure you want to delete these links?');
  
  $form['question'] = array(
    '#markup' => $confirm_question,
  	'#prefix' => '<h1>',
  	'#suffix' => '</h1>',
  );
  $form['downloads'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No content available.'),
  );
  $form['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('temporary_download_multiple_delete_confirm_submit'),
    ),
    'cancel' => array(
      '#type' => 'link',
      '#href' => 'node/'.$node->nid.'/temporary-download', 
      '#title' => t('Cancel'),
      '#attributes' => array('class' => array('button')),
    ),
  );
  
  return $form;
}

function temporary_download_multiple_delete_confirm_submit($form, &$form_state) {

  $node = $form_state['build_info']['args'][0];
  $tokens = array_keys(array_filter($form_state['values']['tokens']));
  
  foreach($tokens as $token) {
    db_delete('temporary_download')->condition('token', $token)->execute();
    db_delete('temporary_download_log')->condition('token', $token)->execute();
  }
  
  drupal_set_message(format_plural(count($tokens)>1, 'Temporary link was deleted', count($tokens).' temporary links have been deleted'));
  
  $form_state['redirect'] = 'node/'.$node->nid.'/temporary-download';
}

function temporary_download_help($path, $arg) {
	return '<p>Help for Temporary File Download.</p>'; 
}