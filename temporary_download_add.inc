<?php

/**
 * @file
 * Generates new temporary download links.
 */

/**
 * Form build function for menu local action.
 *
 * @param $form
 * @param $form_state
 * @param $node
 *   A node with attached files.
 *
 * @return array
 *   A render array.
 */
function temporary_download_add_form($form, $form_state, $node) {
  
  $form = array();
  
  // Build the sortable table header.
  $header = array(
    'filename' => array('data' => t('Filename'), 'field' => 'filename'),
    'description' => array('data' => t('Description'), 'field' => 'description'),
    'scheme' => array('data' => t('Scheme'), 'field' => 'scheme'),
  	'filemime' => array('data' => t('Type'),'field' => 'filemime'),
    'filesize' => array('data' => t('Size'),'field' => 'filesize'),
    'timestamp' => array('data' => t('Uploaded'),'field' => 'timestamp'),
  );

  // Get files from node. // foreach ($files as $fid => $file)
  $files = temporary_download_node_files($node);
  
  // Sort by header.
  // uasort will used for keeping the association between key and val ($fid => $file)
  $ts = tablesort_init($header);
  $sort = ($ts['sort'] == 'asc') ? 1 : -1;
  $field = $ts['sql'];
  switch($field) {
    // alpha numeric fields
    case 'filename':
    case 'filemime':
    case 'description':
    case 'scheme':
    	uasort( $files, function($a,$b) use ($field,$sort) {
        return strcmp($a[$field],$b[$field]) * $sort;
      });
      break;
    // integer fields
    case 'timestamp':
    case 'filesize':
      uasort( $files, function($a,$b) use ($field,$sort) {
        return (intval($a[$field]) - intval($b[$field])) * $sort;
      });
      break;
  }

  // Build the rows of the table.
  $options = array();
  foreach ($files as $fid => $file) {
    $options[$fid] = array(
      'filename' => check_plain($file['filename']),
      'description' => check_plain($file['description']),
    	'scheme' => check_plain($file['scheme']),	
    	'filemime' => check_plain($file['filemime']),
    	'filesize' => format_size($file['filesize']),
      'timestamp' => format_date($file['timestamp'], 'short'),
    );
  }

  $form['files_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('List of available files'),
    'files' => array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No content available.'),
     ),
  );

  // Suggest a usefully default value for the date/time of expiration.
  $default_expires = temporary_download_create_expiration_date(time(), variable_get('temporary_download_expires_interval', TEMPORARY_DOWNLOAD_DEFAULT_EXPIRES_INTERVAL));
 
  $form['expires_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Date/Time of expiration'),
    'expires' => array(
      '#type' => 'date_popup',
      '#default_value' => format_date($default_expires, 'custom', DATE_FORMAT_DATETIME),
      '#date_type' => DATE_DATETIME,
      '#date_timezone' => date_default_timezone(),
    ),
  );

  $form['invitation_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Send invitations to download via email'),
    '#description' => t('Specify the invitees to download by entering of their email addresses. Enter one address per line.'),
    'emails' => array(
      '#type' => 'textarea',
      '#cols' => 60,
      '#rows' => 3,
    ),
  );
  
  $form['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Generate temporary links'),
    ),
    'cancel' => array(
      '#type' => 'link',
      '#href' => 'node/'.$node->nid.'/temporary-download', 
      '#title' => t('Cancel'),
      '#attributes' => array('class' => array('button')),
    ),
  );
  
  $form['pager'] = array('#markup' => theme('pager'));
  
  return $form;
}


/** 
 * Helper function, splits input into separate lines, trim the lines and discard empty lines.
 * 
 * @return array
 *   list of emails
 */ 
function _temporary_download_add_get_emails(&$form_state) {

  $emails = array();

  if (!empty($form_state['values']['emails'])) {
    $emails = preg_split('/\r\n|[\r\n]/', $form_state['values']['emails']);
    array_walk($emails, function(&$val, $key) { $val = trim($val); } );
    $emails = array_filter($emails);
  }
  
  return $emails;
}


/**
 * Form validate function.
 *
 * @param $form
 * @param $form_state
 */
function temporary_download_add_form_validate($form, &$form_state) {
  
  $emails = _temporary_download_add_get_emails($form_state);
  if (!empty($emails)) {
    foreach($emails as $email) {
      if (!valid_email_address($email)) {
        form_set_error(
          'emails', 
          t('Invalid email address: @email', array('@email' => $email))
        );
        break;
      }
    }
  }
}


/**
 * Form submit function.
 *
 * @param $form
 * @param $form_state
 */
function temporary_download_add_form_submit($form, &$form_state) {
  
  $node = $form_state['build_info']['args'][0];
  $fids = array_filter($form_state['values']['files']);
  $emails = _temporary_download_add_get_emails($form_state);
  
  $objExpires = new DateObject( $form_state['values']['expires'], date_default_timezone(TRUE), DATE_FORMAT_DATETIME);
  $expires = $objExpires->format('U');
  
  $files = temporary_download_node_files($node);
  array_filter($files, function($file) use ($fids) { 
  	return in_array($file['fid'],$fids); 
  });
  
  if (!empty($fids)) {
  	foreach($files as $file) {
  		if (!empty($emails)) {
  			foreach($emails as $email) {
  				$token = temporary_download_register_file($file['fid'], $expires, $email, $file);
  				temporary_download_send_mail('invitation', $token);
  			}
  		}
  		else {
  			temporary_download_register_file($file['fid'], $expires, NULL, $file);
  		}
  	}
    $form_state['redirect'] = 'node/'.$node->nid.'/temporary-download';
  }
  else {
    drupal_set_message(t('No files selected.'));
  }
  
}
